const { KohanaJS } = require('kohanajs');

module.exports = {
  databasePath: `${KohanaJS.APP_PATH}/../database`,
  defaultLanguage: 'en',
};
